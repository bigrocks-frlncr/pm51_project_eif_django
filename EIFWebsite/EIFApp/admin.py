from django.contrib import admin
from EIFWebsite.models import *
# Register your models here.

admin.site.register(CMSPlugin)

class NoticiasAdmin(admin.ModelAdmin):
    list_display = ('Titulo', 'Slug', 'Estado','Creado','Categorias','Destacado','Autor')
    list_filter = ("Estado", "Categorias", "Destacado")
    search_fields = ['Titulo', 'Contenido']
    prepopulated_fields = {'Slug': ('Titulo',)}

class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('Titulo', 'Subtitulo')
    #list_filter = ("Titulo",)
    search_fields = ['Titulo', 'Subtitulo']
    prepopulated_fields = {'Slug': ('Titulo',)}

class TagAdmin(admin.ModelAdmin):
    list_display = ('Tag', 'Slug')
    #list_filter = ("Titulo",)
    search_fields = ['Tag']
    prepopulated_fields = {'Slug': ('Tag',)}

admin.site.register(Noticias, NoticiasAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Tag, TagAdmin)