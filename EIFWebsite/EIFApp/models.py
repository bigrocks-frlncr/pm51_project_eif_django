from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.urls import reverse

# Create your models here.

Usuario = get_user_model()

STATUS = (
    (0,"Borrador"),
    (1,"Publicado"),
    (2,"Archivado")
)

class Categoria(models.Model):
    Titulo = models.CharField(max_length=30)
    Subtitulo = models.CharField(max_length=30, blank=True)
    Slug = models.SlugField()
    Imagen = models.ImageField(blank=True)

    def __str__(self):
        return self.Titulo
    
class Publicacion(models.Model):
    Titulo = models.CharField(max_length=200, unique=True)
    Slug = models.SlugField(max_length=200, unique=True)
    #Autor = models.ForeignKey(Usuario, on_delete= models.PROTECT, related_name='blog_posts')
    Actualizado = models.DateTimeField(auto_now= True)
    Descripcion = models.CharField(max_length=255)
    Contenido = models.TextField()
    Creado = models.DateTimeField(auto_now_add=True)
    Categorias = models.ForeignKey(Categoria, on_delete= models.PROTECT, related_name='Categoria')
    Imagen = models.ImageField(blank=True)
    Estado= models.IntegerField(choices=STATUS, default=0)
    Destacado = models.BooleanField(default=0)

    def __str__(self):
        return self.Titulo


class Meta:
    ordering = ['-Creado']

    def __str__(self):
        return self.Titulo