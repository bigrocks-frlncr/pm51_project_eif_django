# Generated by Django 3.1.14 on 2023-03-20 23:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EIFApp', '0002_auto_20230320_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='short_content',
            field=models.CharField(max_length=255),
        ),
    ]
