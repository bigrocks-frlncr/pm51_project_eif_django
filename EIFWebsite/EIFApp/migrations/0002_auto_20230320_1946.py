# Generated by Django 3.1.14 on 2023-03-20 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EIFApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('subtitle', models.CharField(max_length=20)),
                ('slug', models.SlugField()),
                ('thumbnail', models.ImageField(upload_to='')),
            ],
        ),
        migrations.AlterModelOptions(
            name='post',
            options={},
        ),
        migrations.AddField(
            model_name='post',
            name='short_content',
            field=models.TextField(default='Empty'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='thumbnail',
            field=models.ImageField(default='None', upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='categories',
            field=models.ManyToManyField(to='EIFApp.Category'),
        ),
    ]
