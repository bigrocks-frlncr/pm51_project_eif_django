from django.apps import AppConfig


class EIFAppConfig(AppConfig):
    name = 'EIFApp'
