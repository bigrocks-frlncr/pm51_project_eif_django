from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.shortcuts import render
from EIFApp.views import *
from EIFWebsite.models import Noticias

# Create your views here.
from django.views import generic


def noticia(request, Slug):
    noticiap = Noticias.objects.get(Slug=Slug)
    
    context = {
        'noticia': noticiap,
    }
    return render(request, "noticia.html", context)

