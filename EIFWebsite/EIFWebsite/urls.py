from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from EIFApp import views


#PARA BOOTSTRAP GRID BUILDER
# project/urls.py

from cms.cms_wizards import cms_page_wizard, cms_subpage_wizard
from bootstrap_grid_builder.cms_wizards import cms_wizards
from cms.wizards.wizard_pool import wizard_pool

# OVERRIDE CMS WIZARD
wizard_pool.unregister(cms_page_wizard)
wizard_pool.unregister(cms_subpage_wizard)

wizard_pool.register(cms_wizards.cms_page_wizard)
wizard_pool.register(cms_wizards.cms_subpage_wizard)

####

admin.autodiscover()

urlpatterns = [
    path("abx", sitemap, {"sitemaps": {"cmspages": CMSSitemap}}),
    path("admin/", admin.site.urls), 
    path("", include("cms.urls")),
    path('', include('EIFApp.urls')),
    path('<str:Slug>', views.noticia, name="noticiaslug"),
   
    
]

urlpatterns += i18n_patterns(
    )

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
