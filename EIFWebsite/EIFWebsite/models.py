from cms.models.pluginmodel import CMSPlugin
from django.db import models
#from django.utils.timezone.now import timezone
import datetime
from PIL import Image
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from ckeditor.fields import RichTextField 
from django.template.defaultfilters import slugify
from django.urls import reverse


Usuario = get_user_model()

STATUS = (
    (0,"Borrador"),
    (1,"Publicado"),
    (2,"Archivado")
)

class Categoria(CMSPlugin):
    Titulo = models.CharField(max_length=30)
    Subtitulo = models.CharField(max_length=30, blank=True)
    Slug = models.SlugField(max_length=100, unique=True)
    Imagen = models.ImageField(blank=True)

    def __str__(self):
        return self.Titulo

class Tag (CMSPlugin):
    Tag = models.CharField(max_length=50)
    Slug = models.SlugField(max_length=100, unique=True)
    
    def __str__(self):
        return self.Tag
    
class Noticias(CMSPlugin):
    Titulo = models.CharField(max_length=50)
    Subtitulo = models.CharField(max_length=100, blank=True)
    Slug = models.SlugField(max_length=50, unique=True)
    Autor = models.ForeignKey(Usuario, on_delete= models.PROTECT, related_name='blog_posts')
    Descripcion = models.CharField(max_length=255)
    Contenido = RichTextField()
    Categorias = models.ForeignKey(Categoria, on_delete= models.PROTECT, related_name='Categoria')
    Tags = models.ManyToManyField(Tag, blank=True)
    Imagen = models.ImageField(blank=False, upload_to="post", default="placeholder.png")
    Estado= models.IntegerField(choices=STATUS, default=0)
    Destacado = models.BooleanField(default=0)
    Creado = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    Actualizado = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
        return self.Titulo
    
    #def get_absolute_url(self):
    #    return reverse("noticia_detalle", args=[str(self.Slug)])
    
    def save(self, *args, **kwargs):  
        if not self.Slug:
            self.Slug = slugify(self.Titulo)
        return super().save(*args, **kwargs)

class Meta:
    ordering = ['-Creado']

    def __str__(self):
        return self.Titulo   
    