from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import gettext_lazy as _
from .models import *

@plugin_pool.register_plugin
class NoticiasPlugin(CMSPluginBase):    
    model = Noticias
    render_template = "Noticias.html"
    cache = False
    
    def render(self, context, instance, placeholder):
        context = super(NoticiasPlugin, self).render(
            context, instance, placeholder
        )
        context.update(
            {
                "noticias": Noticias.objects.filter(Estado=1).order_by('-Creado')[0:4]
            }
        )
        return context
    
  
@plugin_pool.register_plugin
class NoticiasListadoPlugin(CMSPluginBase):
    model = Noticias
    render_template = "NoticiasListado.html"
    cache = False
    
    def render(self, context, instance, placeholder):
        context = super(NoticiasListadoPlugin, self).render(
            context, instance, placeholder
        )
        context.update(
            {
                "noticiaslistado": Noticias.objects.filter(Estado=1).order_by('-Creado')[0:10]
            }
        )
        return context