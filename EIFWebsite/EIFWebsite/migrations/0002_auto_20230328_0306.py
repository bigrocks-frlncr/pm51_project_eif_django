# Generated by Django 3.1.1 on 2023-03-27 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EIFWebsite', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoriamodel',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='categoriamodel',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
