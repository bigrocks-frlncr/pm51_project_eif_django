# Generated by Django 4.1.7 on 2023-03-30 05:38

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('EIFWebsite', '0009_auto_20230330_0034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticias',
            name='Slug',
            field=models.SlugField(default=uuid.uuid4, max_length=100),
        ),
    ]
